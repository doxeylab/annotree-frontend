export const APP_CLICKED = 'APP_CLICKED';
export const VERSION = '1.0.0';


// available highlight types
export const SEARCH_TAX = 'tax';
export const SEARCH_PFAM = 'pfam';
export const SEARCH_KEGG = 'kegg';
export const SEARCH_ALL = 'all'; // special ALL type
export const AVAILABLE_HIGHLIGHTS = [SEARCH_TAX, SEARCH_PFAM, SEARCH_KEGG];
export const HIGHLIGHT_CLASSES = {
	[SEARCH_TAX]: 'taxHighlighted',
	[SEARCH_PFAM]: 'pfamHighlighted',
	[SEARCH_KEGG]: 'keggHighlighted',
};

const pfamOption = {
  displayText: 'Pfam Family',
  placeholder: 'Searching for genomes containing ALL of given families',
};

const taxOption = {
  displayText: 'Taxonomy',
  placeholder: 'Enter NCBI species ids, or search by species name'
};

const keggOption = {
  displayText: 'KEGG',
  placeholder: 'Enter a comma separated KO number'
};

export const QUERY_BOX_OPTIONS = {
  [SEARCH_TAX]: taxOption,
  [SEARCH_PFAM]: pfamOption,
  [SEARCH_KEGG]: keggOption,
};
