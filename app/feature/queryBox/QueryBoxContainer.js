import React, {PropTypes,Component} from 'react';
import {observer} from "mobx-react";
// import QueryBoxView from './QueryBoxView';
import * as QueryBoxConstant from './QueryBoxConstant';
import * as AppConstant from 'AppConstant';
import AppDispatcher from 'dispatcher/AppDispatcher';
import Tooltip from 'rc-tooltip';
import 'rc-tooltip/assets/bootstrap.css';
import './QueryBox.less';

@observer
export default class QueryBoxContainer extends Component{
  static proptypes = {
    queryBoxStore: React.PropTypes.object.isRequired,
  };
  constructor(props) {
    super(props);
    this.queryBoxStore = props.queryBoxStore;
  }
  handleOptionChanged(e, searchType){
    e.stopPropagation();
    AppDispatcher.handleViewAction({
      type: QueryBoxConstant.QUERY_OPTION_CHANGED,
      payload: searchType
    });
  }
  handleOpenOption(e){
    e.stopPropagation();
    AppDispatcher.handleViewAction({
      type: QueryBoxConstant.QUERY_OPTION_OPENED,
      payload: null
    });
  }
  handleQueryTextChange(e){
    e.stopPropagation();
    var newQueryText = e.target.value;
    AppDispatcher.handleViewAction({
      type: QueryBoxConstant.QUERY_TEXT_CHANGED,
      payload: newQueryText,
    });
  }
  handleSuggestionClicked(e,suggestion){
    e.stopPropagation();
    AppDispatcher.handleViewAction({

      type: QueryBoxConstant.QUERY_SUGGESTION_CLICKED,
      payload: suggestion,
    });
    if(this.queryInput) this.queryInput.focus();
  }
  handleQueryKeyDown(e){
    if (e.keyCode === 13){
      AppDispatcher.handleViewAction({
        type: QueryBoxConstant.QUERY_SUBMITTED,
      });
    }
  }
  handleQueryGoClick(e){
    e.stopPropagation();
    AppDispatcher.handleViewAction({
      type: QueryBoxConstant.QUERY_SUBMITTED,
    });
  }
  handleFileUpload(e){
    e.stopPropagation();
    var files = e.target.files;
    AppDispatcher.handleViewAction({
      type: QueryBoxConstant.FILE_UPLOADED,
      payload: files,
    });
    // clear file input
    this.refs.fileInput.value = null;
  }
  triggerUpload(e){
    this.refs.fileInput.click();
  }
  render(){
    var {selectedOption,optionsShown,options, suggestions, query, warning} = this.queryBoxStore;
    var selectedObj = AppConstant.QUERY_BOX_OPTIONS[selectedOption];
    var self = this;
    return (
        <div className='queryBoxContainer'>
          <div className="">
            <div className='optionContainer'>
              <button className='optionButton' onClick={this.handleOpenOption}> {selectedObj.displayText}
                &nbsp; <span className="fa fa-chevron-down"></span>
              </button>
              {optionsShown && <ul className="optionList">
                            {
                              _.chain(options)
                              .map(function(o, type){
                                return {
                                  'type': type,
                                  'displayText':o.displayText,
                                }
                              })
                              .filter((o)=>o.type!==selectedOption)
                              .map((o)=>
                                (<li className="optionItem" key={o.type} onClick={(e)=>self.handleOptionChanged(e,o.type)}>
                                  {o.displayText}</li>))
                              .value()
                            } 
                          </ul>}
            </div>
            <div className="inputContainer">
              <input value={query} type="text"
                className="input"
                onChange={this.handleQueryTextChange}
                placeholder={selectedObj.placeholder}
                ref={(input) => { this.queryInput = input; }}
                onKeyDown={this.handleQueryKeyDown}/>
              {warning && 
                <span className="warningContainer">
                  <Tooltip
                    animation="zoom"
                    trigger={['hover']}
                    overlayStyle={{ zIndex: 1000 }}
                    overlay={<span>{warning.message}</span>}
                  >
                  <i className="fa fa-warning"></i>
                  </Tooltip>
                </span>}
              {suggestions.length > 0 && <div className="autocompleteContainer">
                {suggestions.map((s,index)=>
                  (<div className="row suggestionEntry" key={index} onClick={(e)=>this.handleSuggestionClicked(e,s)}>
                    <div className="col-xs-9">{s.detail}
                    </div>
                    <div className="col-xs-offset-1 col-xs-2">{s.displayText}
                    </div>
                  </div>))
                }
              </div>}
            </div>
            <button className="goButton" onClick={(e)=>this.handleQueryGoClick(e)}>
              GO
            </button>

            <input type='file'
                    ref='fileInput'
                    onChange={this.handleFileUpload.bind(this)}
                    accept='.xml'
                    className='fileUploader'/>
            {// display the file uploader only if taxonomy is selected
              selectedOption === AppConstant.SEARCH_TAX &&
                <span className='fileUploaderLabel'
                  onClick={this.triggerUpload.bind(this)}>BLAST XML2</span>
            }
          </div>
        </div>
      );
  }
};



