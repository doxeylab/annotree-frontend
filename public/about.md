

About
----

AnnoTree is a web tool to facilitate visualization of genome annotations across large phylogenetic trees. AnnoTree version 1.0 includes [KEGG](https://www.genome.jp/kegg/) and [PFAM](https://pfam.xfam.org/) annotations for nearly 24,000 bacterial genomes. Phylogenetic and taxonomy information is derived from the [GTDB](http://gtdb.ecogenomic.org/) database.

Future releases will include additional types of functional annotations, and expand the phylogenomic framework to include Archaea and Eukaryotes.

Recommended Browser
---
For the best result, we recommend using **Chrome** browser.

Team
---
AnnoTree was developed by the Doxey Lab at the University of Waterloo.
 
* Han Chen (core developer)
* Kerrin Mendler (core developer)
* Donovan Parks (GTDB genomic data and TOL)
* Laura Hug (phylogenomics, case studies, TOL)
* Andrew Doxey (tool conception/design)

Citation
----

Kerrin Mendler, Han Chen, Donovan H Parks, Laura A Hug, Andrew C Doxey. (2018) AnnoTree: visualization and exploration of a functionally annotated microbial tree of life. _bioRxiv_ 463455; doi: https://doi.org/10.1101/463455


Contact us
---
Please email Kerrin Mendler (kemendle at uwaterloo dot ca) or Andrew Doxey (acdoxey at uwaterloo dot ca)

Version
----
Version for this app and associated data are listed under the main app footer.
