Known Issues
===

Squeezed Graphics
----
Try enlarge the browser window if components are squeezed


I encountered an `internal server error` when searching particular pfam families, and the summary panel does not display
----
We might have just launched an update that causes browser cached tree to be out of sync of that in database. Clear browser cache by refreshing `ctrl + shift + R`.

My taxa don't show up in the tree
----
Your taxa will not appear here if it's not included in [GTDB](http://gtdb.ecogenomic.org/), which is our source for taxonomy. If it is in GTDB but not here, we might have used an older version of GTDB.
