import { observable, computed, action } from 'mobx';
import * as AppConstant from 'AppConstant';

export default class QueryBoxStore{
  @observable options = AppConstant.QUERY_BOX_OPTIONS;
  @observable selectedOption = AppConstant.SEARCH_PFAM;
  @observable optionsShown = false;
  @observable suggestions = [];
  @observable query = '';
  /*
    lastSearchedConfig is the last successfully searched setting
    e.g. {
      option: 'pfam',
      query: 'PF00001, PF89333'
    }
  */
  @observable lastSearchedConfig = null;
  @observable warning = null;
  constructor() {
    // don't do any thing
  }

  setLastSearchedConfig(){
    this.lastSearchedConfig = {
      option: this.selectedOption,
      query: this.query
    };
    return this.lastSearchedConfig;
  }

  clearLastSearchedConfig(){
    this.lastSearchedConfig = null;
  }
  // return an array of query phrases from the text
  getQueryPhrases(queryText){
    if (typeof(queryText) === 'undefined') queryText = this.query;
    return queryText.split(',').map((s)=>s.trim()).filter((t)=>t.length>0);
  }

  getTaxIds(queryText){
    if (typeof(queryText) === 'undefined') queryText = this.query;
    return queryText.split(',')
      .map((x)=>x.replace(/ /g,''))
      .filter((x)=>x.length>0 && !x.startsWith('t'))
      .map((x)=>parseInt(x));
  }

  getNodeIds(queryText){
    if (typeof(queryText) === 'undefined') queryText = this.query;
    return queryText // tree node ids that start with 't' in front
          .split(',')
          .map((x)=>x.replace(/ /g,''))
          .filter((x)=>x.length>0 && x.startsWith('t'))
          .map((x)=>parseInt(x.replace('t','')));
  }
}



