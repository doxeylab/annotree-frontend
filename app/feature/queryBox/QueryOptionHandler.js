import * as _ from 'lodash';
import * as QueryBoxConstant from 'queryBox/QueryBoxConstant';
import * as TreeConstant from 'treeOfLife/TreeConstant';
import * as AppConstant from 'AppConstant';

class QueryOptionHandler{
  constructor(props){
    this.queryBoxStore = props.stores.queryBoxStore;
    this.treeStore = props.stores.treeStore;
    this.summaryBoxStore = props.stores.summaryBoxStore;
    this.queryService = props.services.queryService;
  }

  _updateSuggestion(phraseToSearch){
    throw 'umimplemented';
  }
  updateSuggestion = _.debounce(this._updateSuggestion, 200);

  handleQuery(){
    var {treeStore, summaryBoxStore, queryBoxStore} = this;
    treeStore.startLoading();
    summaryBoxStore.clear();
    var lastSearchedConfig = queryBoxStore.setLastSearchedConfig();
    treeStore.lastSearchedConfig = lastSearchedConfig;
    return this._submitQuery()
      .catch(function(err){
        console.log('query encountered error', err);
        var msg = (err && err.error) || 'internal server error';
        queryBoxStore.warning = {
          message: msg,
        };
        // clear lastSearchedConfig since query is unsuccessful
        treeStore.lastSearchedConfig = null;
        queryBoxStore.clearLastSearchedConfig();
      })
      .then(function(){
        treeStore.stopLoading();
      });
  }
};

class TaxonomyQueryOptionHandler extends QueryOptionHandler{
  constructor(props){
    super(props);
  }
  _updateSuggestion(phraseToSearch){
    var self = this;
    if (phraseToSearch === ''){
      return;
    }
    this.queryService.autocompleteTaxonomy(phraseToSearch)
      .then(function(data){
        /* sample data
          [
            {
              "taxId": 9060,
              "species": "Homo Sapien"
            }
          ]
        */
        var serverSuggestions = data?data.map((d)=>{
          return {
            detail: d['species'],
            displayText: d['taxId'],
          };
        }):[];

        // not only look for suggestions from server, but also using loaded tree
        var treeSuggestions = [];
        var nodes = _.values(self.treeStore.masterIdNodeMap);
        for (var i=0;i<nodes.length;i++){
          var node = nodes[i];
          if (node.level && node.level.toLowerCase().startsWith(phraseToSearch) && node.rank != TreeConstant.GENOME){
            treeSuggestions.push({
              detail: node.level + ' (' + node.rank +')',
              displayText: 't' + node.id
            })
          }
          // only look for 3 suggestions
          if (treeSuggestions.length >= 3){
            break;
          }
        }
        var suggestions = treeSuggestions.concat(serverSuggestions);
        // nothing from tree search or server
        if (suggestions.length === 0){
          self.queryBoxStore.suggestions = [QueryBoxConstant.NO_MATCH_SUGGESTION];
          return;
        }
        self.queryBoxStore.suggestions = suggestions;
      });
  }

  _submitQuery(){
    var {queryBoxStore,treeStore,summaryBoxStore, queryService} = this;
    var {query, selectedOption} = queryBoxStore;
    // treeStore.resetToDefaultDisplayable(); don't reset view
    if (!/( *t?\d+ *, *)*/.test(query)){
      var err = {
        error: 'When selecting for taxonomy option, enter comma seperated ncbi taxonomy id. The query is invalid.'
      };
      return Promise.reject(err);
    }
    var taxIds = queryBoxStore.getTaxIds();
    var nodeIds = queryBoxStore.getNodeIds();

    return queryService.getNodeIdByTaxIds(taxIds)
      .then(function(hits){
        hits = hits.concat(nodeIds);
        if (!hits || hits.length === 0){
          throw {
            error: 'Query returned no result.'
          };
        }else{
          var hitData = {};
          hitData[AppConstant.SEARCH_TAX] = hits;
          treeStore.setHits(hitData);
        }
      })
  }
};


class DomainQueryOptionHandler extends QueryOptionHandler{
  constructor(props){
    super(props);
  }
  _updateSuggestion(phraseToSearch){
    var self = this;
    if (phraseToSearch === ''){
      return;
    }
    this.queryService.autocompleteDomain(phraseToSearch)
      .then(function(data){
        /* sample data
          [
            {
              "description": "Forkhead domain", 
              "pfamA_acc": "PF00250", 
              "pfamA_id": "Forkhead"
            }, 
            {
              "description": "Forkhead N-terminal region", 
              "pfamA_acc": "PF08430", 
              "pfamA_id": "Forkhead_N"
            }
          ]
        */
        // note that we don't use pfamA_id
        if (!data || data.length === 0){
          self.queryBoxStore.suggestions = [QueryBoxConstant.NO_MATCH_SUGGESTION];
          return;
        }
        var suggestions = data.map((d)=>{
          return {
            detail: d['description'],
            displayText: d['pfamA_acc'],
          };
        });
        self.queryBoxStore.suggestions = suggestions;
      });
  }

  _submitQuery(phrases){
    var {queryBoxStore,treeStore,summaryBoxStore, queryService} = this;
    var {query, selectedOption} = queryBoxStore;
    var phrases = queryBoxStore.getQueryPhrases();
    if (!phrases || phrases.length === 0){
      var err = {
        error: 'You have entered invalid or empty query.'
      };
      return Promise.reject(err);
    }

    return queryService.queryDomains(phrases)
      .then(function(hits){
        if (hits.length == 0) {
          throw {
            error: 'Query returned no result',
          };
        }
        var hitData = {};
        hitData[AppConstant.SEARCH_PFAM] = hits;
        treeStore.setHits(hitData);
        summaryBoxStore.hits = hits;
        summaryBoxStore.setSummary({
          distributionRank: TreeConstant.PHYLUM,
          distribution: treeStore.getHitDistribution(hits, TreeConstant.PHYLUM),
          queryUsed: queryBoxStore.query,
        });
      })
  }
};

class KEGGQueryOptionHandler extends QueryOptionHandler{
  constructor(props){
    super(props);
  }

  _updateSuggestion(phraseToSearch){
    var self = this;
    if (phraseToSearch === ''){
      return;
    }
    this.queryService.autocompleteKegg(phraseToSearch)
      .then(function(data){
        /* sample data
          [
            {
              "description": "ZDS, crtQ; zeta-carotene desaturase [EC:1.3.5.6]", 
              "keggId": "K00514"
            } ...
          ]
        */
        if (!data || data.length === 0){
          self.queryBoxStore.suggestions = [QueryBoxConstant.NO_MATCH_SUGGESTION];
          return;
        }
        var suggestions = data.map((d)=>{
          return {
            detail: d['description'],
            displayText: d['keggId'],
          };
        });
        self.queryBoxStore.suggestions = suggestions;
      });
  }

  _submitQuery(){
    var {queryBoxStore,treeStore,summaryBoxStore, queryService} = this;
    var {query, selectedOption} = queryBoxStore;
    // treeStore.resetToDefaultDisplayable(); don't reset view
    if (!/^( *K\d{5}(&K\d{5})* *)(, *K\d{5}(&K\d{5})* *)* *,? *$/g.test(query)){
      throw {
        error: 'Invalid query'
      };
    }
    var keggIds = queryBoxStore.getQueryPhrases();

    return queryService.queryKegg(keggIds)
      .then(function(hits){
        if (!hits || hits.length === 0){
          throw {
            error: 'Query returned no result.'
          };
        }else{
          var hitData = {};
          hitData[AppConstant.SEARCH_KEGG] = hits;
          treeStore.setHits(hitData);
          summaryBoxStore.hits = hits;
          summaryBoxStore.setSummary({
            distribution: treeStore.getHitDistribution(hits, TreeConstant.PHYLUM),
            queryUsed: queryBoxStore.query,
            distributionRank: TreeConstant.PHYLUM,
          });
        }
      })
  }
};

const Handlers = {
  initializeOptionHandlers: function(stores, services){
    this._handlers = {};
    this._handlers[AppConstant.SEARCH_TAX] = new TaxonomyQueryOptionHandler({
      stores: stores,
      services: services
    });
    this._handlers[AppConstant.SEARCH_PFAM] = new DomainQueryOptionHandler({
      stores: stores,
      services: services
    });
    this._handlers[AppConstant.SEARCH_KEGG] = new KEGGQueryOptionHandler({
      stores: stores,
      services: services
    });
  },
  selectOptionHandler: function(selectedOption){
    return this._handlers[selectedOption];
  }
};
export default Handlers;
