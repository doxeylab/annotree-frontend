import BaseService from './BaseService';
import * as Config from '../Config';

export default class ConfigService extends BaseService{
  constructor(props) {
    super(props);
  }

  getVersionInfo(){
    var self = this;
    return Promise.all([
        this.getJson('/version',{
          databaseType: '/'+Config.BACTERIAL_DATABASE
        }),
        this.getJson('/version',{
          databaseType: '/'+Config.ARCHAEAL_DATABASE
        })
      ])
      .then(function(versions){
        var result = {};
        result['bacteria'] = {
          'gtdb': versions[0]['gtdb'],
          'pfam': versions[0]['pfam'],
        };
        result['archaea'] = {
          'gtdb': versions[1]['gtdb'],
          'pfam': versions[1]['pfam'],
        };
        return result;
      })
  }
}
