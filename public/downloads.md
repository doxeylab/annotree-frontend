AnnoTree is divided to three parts, frontend, backend and database.

* Database: https://bitbucket.org/doxeylab/annotree-database
* Frontend: https://bitbucket.org/doxeylab/annotree-frontend
* Backend: https://bitbucket.org/doxeylab/annotree-backend

For users wishing to fully install AnnoTree, we provide a [docker](https://www.docker.com/) containerized install at https://bitbucket.org/doxeylab/annotree-docker-compose

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a><br />The data is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
